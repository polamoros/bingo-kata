<?php

use PolAmoros\BingoKata\Controllers\AbstractController;

return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        'storage' => [
            'type' => getenv('STORAGE_TYPE') !== false ? getenv('STORAGE_TYPE') : 'file'
        ],
        'game' => [
            AbstractController::MIN_KEY => 1,
            AbstractController::MAX_KEY => 75,
            AbstractController::BOUNDS_KEY => [
                [
                    AbstractController::MIN_KEY => 1,
                    AbstractController::MAX_KEY => 15,
                ],
                [
                    AbstractController::MIN_KEY => 16,
                    AbstractController::MAX_KEY => 30,
                ],
                [
                    AbstractController::MIN_KEY => 31,
                    AbstractController::MAX_KEY => 45,
                ],
                [
                    AbstractController::MIN_KEY => 46,
                    AbstractController::MAX_KEY => 60,
                ],
                [
                    AbstractController::MIN_KEY => 61,
                    AbstractController::MAX_KEY => 75,
                ]
            ]
        ]
    ],
];
