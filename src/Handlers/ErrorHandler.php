<?php

namespace PolAmoros\BingoKata\Handlers;

use Exception;
use Slim\Http\Request;
use Slim\Http\Response;

class ErrorHandler
{
    const DEFAULT_HTTP_ERROR_CODE = 500;

    /**
     * Parse the exceptions of the API
     *
     * @param Request $request
     * @param Response $response
     * @param Exception $exception
     * @return Response
     */
    public function __invoke(Request $request, Response $response, Exception $exception) : Response
    {
        $data = array(
            'status' => $exception->getCode(),
            'error' => $exception->getMessage()
        );
        $httpCode = self::DEFAULT_HTTP_ERROR_CODE;
        return $response->withJson($data, $httpCode);
    }
}
