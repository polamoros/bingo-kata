<?php

namespace PolAmoros\BingoKata\Controllers;

use Slim\Container;
use PolAmoros\BingoKata\Storages\GameStorageInterface;

abstract class AbstractController
{
    // Keys of the Dependency Container
    const STORAGE_FACTORY_KEY = 'storage';

    const MIN_KEY = 'min';
    const MAX_KEY = 'max';
    const BOUNDS_KEY = 'bounds';
    const WINNER_KEY = 'winner';
    const PENDING_KEY = 'pending';

    // Keys of parameters in URL
    const GAME_ID = 'gameId';
    const CARD_ID = 'cardId';

    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Return a GameStorageInterface instance for the game with id $gameId
     *
     * @param string $gameId
     *
     * @return GameStorageInterface
     */
    protected function getGameStorage(string $gameId) : GameStorageInterface
    {
        $gameStorageSettings = $this->container['settings']['storage'];
        $type = $gameStorageSettings['type'];
        return $this->container[self::STORAGE_FACTORY_KEY]->make($type, $gameId);

    }
}
