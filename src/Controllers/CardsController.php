<?php

namespace PolAmoros\BingoKata\Controllers;

use Exception;
use Slim\Http\Request;
use Slim\Http\Response;

class CardsController extends AbstractController
{
    const INVALID_CARD_SIZE_ERROR = 'Invalid card size for the provided bounds';

    /**
     * Return a new Card with random values and a unique Id
     *
     * @param Request $request      Params in request (getParam)
     *
     * @param Response $response
     * @param array $args           Parameters in the uri
     *                              [
     *                                  self::GAME_ID => $gameId
     *                              ]
     *
     * @return Response
     */
    public function createCard(Request $request, Response $response, array $args) : Response
    {
        $gameId = $args[self::GAME_ID];
        $storage = $this->getGameStorage($gameId);
        $bounds = $this->container['settings']['game'][AbstractController::BOUNDS_KEY];

        $values = $this->generateRandomCard($bounds);
        $id = $storage->createCard($values);
        $data = [
            'id' => $id,
            'values' => $values,
        ];
        return $response->withJson($data);
    }

    /**
     * Return the Card info
     *
     * @param Request $request      Params in request (getParam)
     *
     * @param Response $response
     * @param array $args           Parameters in the uri
     *                              [
     *                                  self::GAME_ID => $gameId,
     *                                  self::CARD_ID => $cardId,
     *                              ]
     *
     * @return Response
     */
    public function getCard(Request $request, Response $response, array $args) : Response
    {
        $gameId = $args[self::GAME_ID];
        $cardId = $args[self::CARD_ID];
        $storage = $this->getGameStorage($gameId);
        $data = [
            'id' => $cardId,
            'values' => $storage->getCard($cardId),
        ];
        return $response->withJson($data);
    }

    /**
     * Check the Card results
     *
     * @param Request $request      Params in request (getParam)
     *
     * @param Response $response
     * @param array $args           Parameters in the uri
     *                              [
     *                                  self::GAME_ID => $gameId,
     *                                  self::CARD_ID => $cardId,
     *                              ]
     *
     * @return Response
     */
    public function checkCard(Request $request, Response $response, array $args) : Response
    {
        $gameId = $args[self::GAME_ID];
        $cardId = $args[self::CARD_ID];
        $storage = $this->getGameStorage($gameId);

        $card = $storage->getCard($cardId);
        $played = $storage->getPlayedNumbers();
        $pending = $this->getPendingNumbers($card, $played);
        $data = [
            'winner' => empty($pending),
            'pending' => $pending,
        ];
        return $response->withJson($data);
    }

    /**
     * Return a matrix[$size][$size] with random values
     * fixed by the min and max bounds.
     *
     * @param array $bounds     List of min and max bounds for each column
     *                          [
     *                              [
     *                                  AbstractController::MIN_KEY = $min,
     *                                  AbstractController::MAX_KEY = $max,
     *                              ],
     *                              ...
     *                          ]
     * @param integer $size
     *
     * @return array
     */
    protected function generateRandomCard(array $bounds, int $size = 5) : array
    {
        if (count($bounds) < $size) {
            throw new Exception(self::INVALID_CARD_SIZE_ERROR);
        }
        $values = array_fill(0, $size, array_fill(0, $size, ''));
        for ($i = 0; $i < $size; ++$i) {
            for ($j = 0; $j < $size; ++$j) {
                $values[$i][$j] = rand(
                    $bounds[$j][AbstractController::MIN_KEY],
                    $bounds[$j][AbstractController::MAX_KEY]
                );
            }
        }
        $values[(int)($size/2)][(int)($size/2)] = '';
        return $values;
    }

    /**
     * Return the pending values of the card
     *
     * @param array $card        example:
     *                          [
     *                              [
     *                                  0 => 10,
     *                                  1 => 20,
     *                                  2 => 30,
     *                              ],
     *                              [
     *                                  0 => 11,
     *                                  1 => 21,
     *                                  2 => 31,
     *                              ],
     *                              [
     *                                  0 => 12,
     *                                  1 => 22,
     *                                  2 => 32,
     *                              ]
     *                          ]
     * @param array $played     example:
     *                          [
     *                              10, 20, 30, 31, 33
     *                          ]
     * @return array            [
     *                              $num1,
     *                              $num2,
     *                              ...
     *                          ]
     */
    protected function getPendingNumbers(array $card, array $played) : array
    {
        $pending = [];
        foreach ($card as $row) {
            foreach ($row as $value) {
                if ($value !== '' && !in_array($value, $played)) {
                    $pending[] = $value;
                }
            }
        }
        return $pending;
    }
}
