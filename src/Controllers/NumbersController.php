<?php

namespace PolAmoros\BingoKata\Controllers;

use Exception;
use Slim\Http\Request;
use Slim\Http\Response;

class NumbersController extends AbstractController
{
    const NO_NUMBER_TO_PLAY_ERROR = 'There are no numbers to play';

    /**
     * Return a random number not repeated in the game
     *
     * @param Request $request      Params in request (getParam)
     *
     * @param Response $response
     * @param array $args           Parameters in the uri
     *                              [
     *                                  self::GAME_ID => $gameId
     *                              ]
     *
     * @return Response
     */
    public function getNumber(Request $request, Response $response, array $args) : Response
    {
        $gameId = $args[self::GAME_ID];
        $storage = $this->getGameStorage($gameId);
        $excluded = $storage->getPlayedNumbers();

        $min = $this->container['settings']['game'][AbstractController::MIN_KEY];
        $max = $this->container['settings']['game'][AbstractController::MAX_KEY];
        $number = $this->getRandomNumber($min, $max, $excluded);
        $storage->setPlayedNumber($number);

        $data = [
            'number' => $number,
            'played' => $excluded,
        ];
        return $response->withJson($data);
    }

    /**
     * Return a random value (min <= x <= max) not included in the $excluded list
     *
     * @param integer   $min        Min value for the random value
     * @param integer   $max        Max value for the random value
     * @param array     $excluded   List of values to exclude
     *
     * @return integer
     */
    protected function getRandomNumber(int $min, int $max, array $excluded) : int
    {
        $range = range($min, $max);
        $values = array_diff($range, $excluded);
        if (empty($values)) {
            throw new Exception(self::NO_NUMBER_TO_PLAY_ERROR);
        }
        return $values[array_rand($values)];
    }
}
