<?php

namespace PolAmoros\BingoKata\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;

class DefaultController extends AbstractController
{

    const DOCUMENTATION_NOT_FOUND_ERROR = "The documentation of the API was not found";
    const API_DOC_PATH = __DIR__ . '/../../doc/swagger.json';

    /**
     * Return a dummy response
     * Used to check if the application is up and running
     *
     * @param Request   $request
     * @param Response  $response
     *
     * @return Response
     */
    public function health(Request $request, Response $response)
    {
        $this->container['logger']->info("Slim-Skeleton '/' health");
        $data = [
            'status' => 200,
            'args' => $request->getQueryParams(),
        ];
        return $response->withJson($data);
    }

     /**
     * Get the yml documentation of the API
     *
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function getDocumentation(Request $request, Response $response) : Response
    {
        if (file_exists(self::API_DOC_PATH)) {
            return $response->withHeader('Content-Type', 'text/plain')->write(
                file_get_contents(self::API_DOC_PATH)
            );
        } else {
            throw new Exception(self::DOCUMENTATION_NOT_FOUND_ERROR);
        }
    }
}
