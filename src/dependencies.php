<?php

use PolAmoros\BingoKata\Storages\GameStorageFactory;
use PolAmoros\BingoKata\Storages\FileGameStorage;
use PolAmoros\BingoKata\Controllers\AbstractController;

// DIC configuration

$container = $app->getContainer();

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

$container[AbstractController::STORAGE_FACTORY_KEY] = function ($c) {
    return new GameStorageFactory();
};
