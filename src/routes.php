<?php

use PolAmoros\BingoKata\Controllers\CardsController;
use PolAmoros\BingoKata\Controllers\DefaultController;
use PolAmoros\BingoKata\Controllers\NumbersController;

$app->get('/health', DefaultController::class . ':health');

$app->group('/v1', function () {
    $this->get('/documentation', DefaultController::class . ':getDocumentation');

    $this->group('/games/{gameId}', function () {

        /**
         * @api {get}       /v1/games/{gameId}/number
         *                  Return a unique random value for the game
         *
         * @urlParam        {gameId}        required
         */
        $this->get('/number', NumbersController::class . ':getNumber');

        /**
         * @api {post}      /v1/games/{gameId}/cards
         *                  Return a new Card with random values and a unique Id
         *
         * @urlParam        {gameId}        required
         */
        $this->post('/cards', CardsController::class . ':createCard');

        /**
         * @api {get}      /v1/games/{gameId}/cards/{cardId}
         *                  Return the Card values
         *
         * @urlParam        {gameId}        required
         * @urlParam        {cardId}        required
         */
        $this->get('/cards/{cardId}', CardsController::class . ':getCard');

        /**
         * @api {get}       /v1/games/{gameId}/cards/{cardId}/check
         *                  Check the card results
         *
         * @urlParam        {gameId}        required
         * @urlParam        {cardId}        required
         */
        $this->get('/cards/{cardId}/check', CardsController::class . ':checkCard');
    });
});
