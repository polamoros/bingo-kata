<?php

namespace PolAmoros\BingoKata\Storages;

use Exception;
use PolAmoros\BingoKata\Storages\FileGameStorage;
use PolAmoros\BingoKata\Storages\GameStorageInterface;

class GameStorageFactory
{
    const FILE_STORAGE = 'file';
    const INVALID_TYPE_EXCEPTION = 'Invalid type';

    /**
     * Return an instance of a class implementing the GameStorageInterface
     *
     * @param string $type
     * @param string $gameId
     *
     * @return GameStorageInterface
     */
    public function make(string $type, string $gameId) : GameStorageInterface
    {
        if ($type === self::FILE_STORAGE) {
            return new FileGameStorage($gameId);
        } else {
            throw new Exception(self::INVALID_TYPE_EXCEPTION);
        }
    }
}
