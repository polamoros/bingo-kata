<?php

namespace PolAmoros\BingoKata\Storages;

interface GameStorageInterface
{
    /**
     * Return the list of played numbers
     *
     * @return array
     */
    public function getPlayedNumbers() : array;

    /**
     * Store a new number in the played numbers list
     *
     * @param int $number
     *
     * @return void
     */
    public function setPlayedNumber(int $number) : void;

    /**
     * Create and store a new card with the given values
     *
     * @param array $values Rows of the new card
     *
     * @return string
     */
    public function createCard(array $values) : string;

    /**
     * Get the card values
     *
     * @param string $id
     *
     * @return string
     *
     * @throws Exception Card not found
     */
    public function getCard(string $id) : array;
}
