<?php

namespace PolAmoros\BingoKata\Storages;

use Exception;

class FileGameStorage implements GameStorageInterface
{
    const BASE_PATH = '/tmp';
    const PLAYED_KEY = 'played';
    const CARDS_KEY = 'cards';

    const INVALID_CARD_ID_ERROR = 'Card not found';

    protected $gameId;
    protected $path;

    public function __construct(string $gameId)
    {
        $this->gameId = $gameId;
        $this->path = self::BASE_PATH . '/game_' . $gameId . '.json';
    }

    /**
     * @inheritDoc
     */
    public function getPlayedNumbers() : array
    {
        $gameData = $this->getGameData();
        if (!isset($gameData[self::PLAYED_KEY])) {
            $gameData[self::PLAYED_KEY] = [];
        }
        return array_keys($gameData[self::PLAYED_KEY]) ?? [];
    }

    /**
     * @inheritDoc
     */
    public function setPlayedNumber(int $number) : void
    {
        $gameData = $this->getGameData();
        $gameData[self::PLAYED_KEY][$number] = true;
        $this->setGameData($gameData);
    }

    /**
     * @inheritDoc
     */
    public function createCard(array $values) : string
    {
        $gameData = $this->getGameData();
        $cardId = uniqid();
        $gameData[self::CARDS_KEY][$cardId] = $values;
        $this->setGameData($gameData);
        return $cardId;
    }

    /**
     * @inheritDoc
     */
    public function getCard(string $id) : array
    {
        $gameData = $this->getGameData();
        if (!isset($gameData[self::CARDS_KEY][$id])) {
            throw new Exception(self::INVALID_CARD_ID_ERROR);
        }
        return $gameData[self::CARDS_KEY][$id];
    }

    protected function getGameData() : array
    {
        if (file_exists($this->path)) {
            return json_decode(file_get_contents($this->path), true);
        }
        return [];
    }

    protected function setGameData(array $data)
    {
        file_put_contents($this->path, json_encode($data));
    }
}
