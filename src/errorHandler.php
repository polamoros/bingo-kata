<?php

use PolAmoros\BingoKata\Handlers\ErrorHandler;

$container = $app->getContainer();
$container['errorHandler'] = function ($c) {
    return new ErrorHandler();
};
