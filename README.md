# Bingo Kata

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites
Before starting, you need to have [PHP 7.2](http://php.net/manual/en/install.php), [Composer](https://getcomposer.org/download/) and [Docker](https://docs.docker.com/install/) installed on your machine.

### Installing

Use the next command to install the project dependencies.

```
composer install
```

## Running the app

You can run this app:

### With docker
```
composer docker:run
```

### Without docker
```
composer run
```

You can check if the app is running here [localhost:8080/health](http://localhost:8080/health). Should return:
```
{
    "status":200,
    "args":[]
}
```

## API documentation

You can use the provided swagger-ui docker container to setup the UI with the API documentation.
```
composer doc:ui
```

### Endpoints

Get a unique random number for the game `gameId`:

`GET /v1/games/{gameId}/number`

Create a card with random values:

`POST /v1/games/{gameId}/card`

Get the card values:

`GET /v1/games/{gameId}/card/{cardId}`

Check card results:

`GET /v1/games/{gameId}/card/{cardId}/check`


## Running the tests

Use the next command to execute the project tests.
```
composer test
```

## Built With

* [Docker](https://www.docker.com/) - Where application will be executed
* [Slim-framework](http://www.slimframework.com) - The web framework used
* [agilekatas](https://agilekatas.co.uk/katas/Bingo-Kata) - Kata description
