#!/usr/bin/env bash

uiPort=3000
apiPort=8080
name=bingo-kata-ui

docker container rm -f $name
docker run -d --name $name -p $uiPort:8080 -e API_URL=http://localhost:$apiPort/v1/documentation swaggerapi/swagger-ui:v3.20.4
docker-compose up -d

echo -e "\nBingo Kata documentation: http://localhost:$uiPort"
