<?php

namespace PolAmoros\BingoKata\Tests\Unit;

use Exception;
use PHPUnit\Framework\TestCase;
use PolAmoros\BingoKata\Storages\FileGameStorage;

class FileGameStorageTest extends TestCase
{
    /**
     * @dataProvider playedNumbersProvider
     */
    public function testGetPlayedNumbers($values, $expected)
    {
        $storage = new FileGameStorage(uniqid());
        foreach ($values as $value) {
            $storage->setPlayedNumber($value);
        }
        $this->assertEquals($expected, $storage->getPlayedNumbers());
    }

    /**
     * @expectedException     TypeError
     */
    public function testInvalidNumber()
    {
        $storage = new FileGameStorage(uniqid());
        $storage->setPlayedNumber('not a number');
    }

    /**
     * @dataProvider cardValuesProvider
     */
    public function testCreateCard($values)
    {
        $storage = new FileGameStorage(uniqid());
        $id = $storage->createCard($values);
        $this->assertEquals($values, $storage->getCard($id));
    }

    public function playedNumbersProvider()
    {
        return [
            'Basic test' => [
                'values'    => [1, 2, 3, 4, 5, 6, 7, 8],
                'expected'  => [1, 2, 3, 4, 5, 6, 7, 8],
            ],
            'Repeated values' => [
                'values'    => [1, 1, 2, 2, 3, 3, 4, 4],
                'expected'  => [1, 2, 3, 4]
            ],
        ];
    }

    public function cardValuesProvider()
    {
        return [
            'Basic card test' => [
                'values' => [
                    [1, 20, 30],
                    [5, '', 31],
                    [10, 22, 32],
                ]
            ]
        ];
    }
}
