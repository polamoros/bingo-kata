<?php

namespace PolAmoros\BingoKata\Tests\Unit;

use Exception;
use PHPUnit\Framework\TestCase;
use PolAmoros\BingoKata\Storages\FileGameStorage;
use PolAmoros\BingoKata\Storages\GameStorageFactory;

class GameStorageFactoryTest extends TestCase
{
    protected $factory;

    public function setUp()
    {
        $this->factory = new GameStorageFactory();
    }

    public function testMakeFileStorageInstance()
    {
        $this->assertInstanceOf(
            FileGameStorage::class,
            $this->factory->make(GameStorageFactory::FILE_STORAGE, 'test')
        );
    }

    /**
     * @expectedException     Exception
     */
    public function testMakeInvalidStorageInstance()
    {
        $this->factory->make('InvalidType', 'test');
    }

}
