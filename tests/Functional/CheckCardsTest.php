<?php

namespace PolAmoros\BingoKata\Tests\Functional;

use PolAmoros\BingoKata\Controllers\AbstractController;

class CheckCardsTest extends BaseTestCase
{
    public function testCheckCard()
    {
        $gameId = uniqid();
        $response = $this->runApp('POST', "/v1/games/{$gameId}/cards");
        $this->assertEquals(200, $response->getStatusCode());
        $card = $this->parseResponse($response);
        $cardId = $card['id'];

        // Get all numbers
        $min = $this->settings['game'][AbstractController::MIN_KEY];
        $max = $this->settings['game'][AbstractController::MAX_KEY];
        for ($i = $min; $i <= $max; ++$i) {
            $response = $this->runApp('GET', "/v1/games/{$gameId}/number");
            $this->assertEquals(200, $response->getStatusCode());
            $result = $this->parseResponse($response);
            $played = $result['played'];

            $response = $this->runApp('GET', "/v1/games/{$gameId}/cards/{$cardId}/check");
            $check = $this->parseResponse($response);
            $this->assertEmpty(array_intersect($check[AbstractController::PENDING_KEY], $played));
        }

        $response = $this->runApp('GET', "/v1/games/{$gameId}/cards/{$cardId}/check");
        $this->assertEquals(200, $response->getStatusCode());
        $check = $this->parseResponse($response);
        $this->assertTrue($check[AbstractController::WINNER_KEY]);
        $this->assertEmpty($check[AbstractController::PENDING_KEY]);
    }
}
