<?php

namespace PolAmoros\BingoKata\Tests\Functional;

use Exception;
use PolAmoros\BingoKata\Controllers\AbstractController;

class GetNumbersTest extends BaseTestCase
{
    /**
     * Test getting all different values
     */
    public function testGetAllNumbers()
    {
        $gameId = uniqid();
        $min = $this->settings['game'][AbstractController::MIN_KEY];
        $max = $this->settings['game'][AbstractController::MAX_KEY];

        $values = [];
        $result = [];
        for ($i = $min; $i <= $max; ++$i) {
            $response = $this->runApp('GET', "/v1/games/{$gameId}/number");
            $this->assertEquals(200, $response->getStatusCode());
            $result = $this->parseResponse($response);
            $number = $result['number'];
            $this->assertNotContains($number, $values);
            $values[] = $number;
        }
        // Add the last value
        $result['played'][] = $number;
        $this->assertEquals($values, $result['played']);
    }

    /**
     * Test error returned when all numbers are returned and the endpoint is called gain
     */
    public function testErrorWhenAllNumbersReturned()
    {
        $gameId = uniqid();
        $min = $this->settings['game']['min'];
        $max = $this->settings['game']['max'];

        for ($i = $min; $i <= $max; ++$i) {
            $response = $this->runApp('GET', "/v1/games/{$gameId}/number");
        }
        $response = $this->runApp('GET', "/v1/games/{$gameId}/number");
        $this->assertEquals(500, $response->getStatusCode());
    }
}
