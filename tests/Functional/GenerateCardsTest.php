<?php

namespace PolAmoros\BingoKata\Tests\Functional;

use Exception;
use PolAmoros\BingoKata\Controllers\AbstractController;

class GenerateCardsTest extends BaseTestCase
{
    public function testValidCard()
    {
        $gameId = uniqid();
        $response = $this->runApp('POST', "/v1/games/{$gameId}/cards");
        $this->assertEquals(200, $response->getStatusCode());
        $card = $this->parseResponse($response);

        $id = $card['id'];
        $values = $card['values'];
        $this->assertTrue(is_array($values) && count($values) === 5);

        $bounds = $this->settings['game'][AbstractController::BOUNDS_KEY];
        $emptyFound = 0;
        foreach ($values as $rowKey => $row) {
            $this->assertTrue(is_array($row) && count($row) === 5);
            foreach ($row as $columnKey => $value) {
                if ($value !== '') {
                    $this->assertTrue(
                        $value >= $bounds[$columnKey][AbstractController::MIN_KEY] &&
                        $value <= $bounds[$columnKey][AbstractController::MAX_KEY]
                    );
                } else {
                    ++$emptyFound;
                }
            }
        }
        $this->assertTrue($emptyFound === 1);
        $this->assertEmpty($values[2][2]);
    }
}
